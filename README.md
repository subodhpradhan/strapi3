# tc-backend-poc with strapi

#### Type Following commands within project directory

### Initialize node modules
### `yarn install`

### Start project in watch mode
### `yarn develop`

### Start project without watch mode
### `yarn start`

### Build Strapi admin panel.
### `yarn build`

Project can be accessed from:
#### [http://localhost:1337/admin](http://localhost:1337/admin)

### Default Admin Credentials
Username: subodh 

Password: subodh

### Required 
Node Version - 10 or above 

yarn package manager

### Documentation
#### [Strapi 3.0.0](https://strapi.io/documentation/3.0.0-beta.x)

### Developers Note
#### Sqlite3 is used currently for database as database is just for poc and it need to be easily synced.
